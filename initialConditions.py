#
# UEA Quantum Fluids group
#
# Routines to create two-dimensional initial conditions
#
# Boundary conditions: periodic
#


# Load python modules
import numpy as np 


# Set values for Pade' approximant of single vortex solution
a1=11./32.
a2=11./384.
b1=1./3.
b2=a2

def vortexPade(rSquare, theta, windingNumber):
    """
        Compute the two-dimensional vortex solution using the Pade' approximants
        
        Parameters
        ----------
        rSquare: double (numpy array)
            radial squared distance from the vortex point
        theta: double (numpy array)
            angular variable about the vortex point
        windingNumber: integer
            winding number of the vortex point
        
        Returns
        ----------
        vortexField: complex (numpy array)
            complex value of the vortex solution
    """
    
    return np.multiply(np.sqrt(rSquare*(a1+a2*rSquare)/(1.+b1*rSquare+b2*np.square(rSquare))), np.exp(1j*windingNumber*theta))



def vortexPoints2D(lx, ly, nx, ny, x, y, vortexPositions=None, vortexWindingNumbers=None, images=3, psi=None):
    """
        Set the initial conditions containing point vortices in two dimensions
        
        Parameters
        ----------
        lx: double
            spatial domain length in the x-direction
        ly: double
            spatial domain length in the y-direction
        nx: integer
            number of uniform points along the x-axis
        ny: integer
            number of uniform points along the y-axis
        x:  double numpy array
            array containing the uniform grid points in the x-direction
        y:  double numpy array
            array containing the uniform grid points in the y-direction
       vortexPositions: numpy array list (None, setting 4 vortices at random positions)
            list of array(s) containing the (x, y) coordinates of the vortex points (the sum must be multiple of 4 if periodic boundary conditions are considered)
        vortexWindingNumbers: numpy array list (None, setting 2 clockwise and 2 anti-clockwise)
            list of array(s) containing the winding number of the vortex points (the sum must be 0 if periodic boundary conditions are considered)
        images: integer (3)
            number of point vortex images considered to match the boundary conditions
        psi: complex numpy array (None)
            psi field to which the vortex points will be added (by multiplication)
        
        Returns
        ----------
        psi: numpy complex array
            initial condition psi field
    """
    
    if vortexPositions==None and vortexWindingNumbers==None:
        vortexPositions=[]
        vortexWindingNumbers=[]
        winding=1
        for l in range(0, 4):
            temp=np.random.random((2))*np.array([lx, ly])
            vortexPositions.append(temp)
            vortexWindingNumbers.append(winding)
            winding=-winding

    if len(vortexPositions)!=len(vortexWindingNumbers):
        print('Error in vortexPoints2D: the lengths of vortexPositions and vortexWindingNumbers do not match')
        return
    
    if len(vortexPositions)%2==0:
        if psi is None:
            psi=np.ones((nx, ny))+1j*np.zeros((nx, ny))
                
        for l in range(0, len(vortexPositions)):
            print('vortex ', l, 'at position ', vortexPositions[l])
            for i in range(-images, images+1):
                for j in range(-images, images+1):
                    xPos=vortexPositions[l][0]+i*lx
                    yPos=vortexPositions[l][1]+j*ly

                    meshX=np.kron(x-xPos, np.ones(ny)).reshape(nx, ny)
                    meshY=np.kron(np.ones(nx), y-yPos).reshape(nx, ny)
                    rSquare=np.square(meshX)+np.square(meshY)
                    theta=np.arctan2(meshX, meshY)
                    psi=psi*vortexPade(rSquare, theta, vortexWindingNumbers[l])
                   
        del meshX
        del meshY
        del rSquare
        del theta
        return psi
            
    else:
        print('Error in vortexPoints2D: the number of vortices is not a multiple of two as requested by periodic boundary conditions')
        return
    


def vortexDipole2D(lx, ly, nx, ny, x, y, dipolePositions=None, dipoleWindingNumbers=None, d=None, psi=None):
    """
        Set the initial conditions containing a dipole in 2d, oriented running parallel to the x-axis.

        Parameters
        ----------
        lx: double
            spatial domain length in the x-direction
        ly: double
            spatial domain length in the y-direction
        nx: integer
            number of uniform points along the x-axis
        ny: integer
            number of uniform points along the y-axis
        x:  double numpy array
            array containing the uniform grid points in the x-direction
        y:  double numpy array
            array containing the uniform grid points in the y-direction
        dipolePositions: numpy array list 
            list of array(s) containing the (x, y) coordinates of the centre of the dipole vortices
        dipoleWindingNumbers: numpy array list 
            list of array(s) containing the winding number of the vortices within the dipoles
        d:  float
            the distance between the two vortices    
        psi: complex numpy array (None)
            psi field to which the dipoles will be added (by multiplication)
        
        Returns
        ----------
        psi: numpy complex array
            initial condition psi field  
    """     
    for l in range(0, len(dipolePositions)):
        ypos=[dipolePositions[l][1]-d/2, dipolePositions[l][1]+d/2]            

        for i in range(0, len(ypos)):
            xPos=dipolePositions[l][0]
            yPos=ypos[i]
        
            meshX=np.kron(x-xPos, np.ones(ny)).reshape(nx, ny)
            meshY=np.kron(np.ones(nx), y-yPos).reshape(nx, ny)
            rSquare=np.square(meshX)+np.square(meshY)
            theta=np.arctan2(meshX, meshY)
            psi=psi*vortexPade(rSquare, theta, dipoleWindingNumbers[l][i])
                   
    del meshX
    del meshY
    del rSquare
    del theta
    return psi




