#
# Newton-Raphson method to find travelling wave solutions with velocity v
# as stationary solutions in the Galilean-boosted frame, that is solving
# the one-dimensional nonlinear Schroedinger (NLS) equation,
# \beta\frac{\partial^2\psi}{\partial x^2} + \alpha|\psi|^2\psi + \mu\psi -iv\partial\psi= 0,
# with no external potential and periodic boundary conditions.
#
# NR method: biconjugate gradient stabilised
# Boundary conditions: periodic
#
# Author:
# Davide Proment, d.proment@uea.ac.uk
# School of Mathematics, University of East Anglia, School of Mathematics
# January 2022
#
# Copyright (C) 2022 Davide Proment, d.proment@uea.ac.uk
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


# Load python modules
import numpy as np
from scipy.sparse import diags
from scipy.sparse.linalg import LinearOperator
from scipy.sparse.linalg import bicgstab
import os
import shutil
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt


# Set simulation parameters
alpha=-1.    						# nonlinear coefficient (set as this to use Pade' coefficients from Berloff)
beta=1. 							# linear coefficient (set as this to use Pade' coefficients from Berloff)
mu=1.                               # chemical potential (to set the value of the solution at infinity)
v=0.3                               # velocity of the Galilean boost

nx=256                              # number of uniform points along the x-axis
lx=100.                             # spatial domain length in the x-direction

dt=1.                               # (fake) time-step
initialStep=0                       # initial step
finalStep=10                     # final step (maximum number of steps of the NR)
tolerance=1e-08                     # tolerance (norm)

conSave=1                           # steps between successive save of conserved quantities
psiSave=1                          # steps between successive save of field psi

dirOutputs='./outputs'                  # directory where to store the outputs
dirFields='%s/fields' % (dirOutputs)   # directory where to store the binary fields
dirPlots='%s/plots' % ( dirOutputs)    # directory where to store the plots

print('parameters set')


# Set other constants
dx=lx/nx                                        # uniform grid step along the x-axis
x=np.linspace(0., lx, num=nx, endpoint=False)   # grid points along the x-axis 

dkx=2.*np.pi/lx                                     # uniform grid step along the kx-axis in Fourier                   
kx=np.fft.ifftshift(dkx*np.arange(-nx/2, nx/2))     # grid points along the kx-axis in Fourier (reordered)

print('other constants set')


# Define (and eventually create) the auxiliary directories where to store outputs and plots
if not os.path.exists(dirOutputs):
    os.makedirs(dirOutputs)
if not os.path.exists(dirFields):
    os.makedirs(dirFields)
if not os.path.exists(dirPlots):
    os.makedirs(dirPlots)

print('auxiliary directories created')


# Create the kx^2 array
kSquare=np.square(kx)

print('kSquare array created')


# Define linear operator for bicgstab givng A.v
#def A(vv, psi=np.zeros((nx), dtype=complex), kSquare=kSquare, nx=nx, alpha=alpha, beta=beta, mu=mu):
def A(vv):
    print('psi=', psi[0:10])
    #print(np.linalg.norm(psi))
    temp=np.fft.fft(vv[0:nx])
    temp=v*kx*temp-beta*kSquare*temp
    temp=np.fft.ifft(temp)
    #print(np.linalg.norm(temp))
    tempStar=np.fft.fft(vv[nx:])
    tempStar=-v*kx*tempStar-beta*kSquare*tempStar
    tempStar=np.fft.ifft(tempStar)
    #
    #Atemp=np.array([
    #    np.concatenate((temp+2*alpha*np.conj(psi)*psi+mu, alpha*np.square(psi))), 
    #    np.concatenate((alpha*np.square(np.conj(psi)), tempStar+2*alpha*np.conj(psi)*psi+mu))])
    '''
    mainDiag=np.concatenate((temp+2*alpha*np.conj(psi)*psi+mu, tempStar+2*alpha*np.conj(psi)*psi+mu))
    upperDiag=alpha*np.square(psi)
    lowerDiag=alpha*np.square(np.conj(psi))
    diagonals=[mainDiag, upperDiag, lowerDiag]
    Atemp=diags(diagonals, offsets=[0, nx, -nx])

    #print(temp.shape, tempStar.shape)
    #print(Atemp.shape, vv.shape)
    #print(Atemp.dtype, vv.dtype)
    #print(Atemp.dot(vv).shape, Atemp.dot(vv).dtype)
    return Atemp.dot(vv)
    '''
    temp=temp+(2*alpha*np.conj(psi)*psi+mu)*vv[0:nx]+alpha*np.square(psi)*vv[nx:]
    tempStar=alpha*np.square(np.conj(psi))*vv[0:nx]+tempStar+(2*alpha*np.conj(psi)*psi+mu)*vv[nx:]
    return np.concatenate((temp, tempStar))

print('linear operator A defined')

# Define functional F for righ hand side of bicgstab
def F(psi=np.zeros((nx), dtype=complex), kSquare=kSquare, nx=nx, alpha=alpha, beta=beta, mu=mu):
    temp=np.fft.fft(psi)
    temp=v*kx*temp-beta*kSquare*temp
    temp=np.fft.ifft(temp)
    temp=temp+(alpha*np.conj(psi)*psi+mu)*psi
    return -np.concatenate((temp, np.conj(temp)))

print('lin_exp array defined')


# Set initial condition

'''
# create a mono-crhomatic wave function whose envelop is slightly perturbed
nCarrier=3              # harmonic number of the mono-chormatic carrier
amplCarrier=1.          # wave amplitude of the mono-chormatic carrier

nPert=10                # harmonic number of the perturbation
amplPert=0.01           # wave amplitude of the perturbation

kCarrier=nCarrier*dkx
kPert=nPert*dkx

psi=np.sqrt(amplCarrier+amplPert*np.cos(kPert*x))*np.exp(1j*kCarrier*x)
'''

psi=1.-0.3*np.exp(-np.square(x-lx/2.)/4.)

print('initial condition for psi set')


# Erase previous simulation conserved quantities
filename='%s/Mass.txt' % (dirOutputs)
if os.path.isfile(filename):
    os.remove(filename)

filename='%s/Momentum.txt' % (dirOutputs)
if os.path.isfile(filename):
    os.remove(filename)

filename='%s/Energy.txt' % (dirOutputs)
if os.path.isfile(filename):
    os.remove(filename)


# Save conserved quantities
step=initialStep
if np.mod(step, conSave)==0:
    
    # Mass
    mass=np.sum(np.abs(psi)**2)*dx
    
    # Nonlinear energy
    nl_energy=-(alpha/2.)*np.sum(np.abs(psi)**4)*dx
        
    # Transform forward into Fourier space (in-place transform)
    psi=np.fft.fft(psi)
        
    # Linear energy
    lin_energy=beta*np.sum(kSquare*np.abs(psi)**2)*dx
        
    # Linear momentum components
    px=beta*np.sum(kx*np.abs(psi)**2)*dx
        
    # Transform backward into physical space (in-place transform)
    psi=np.fft.ifft(psi)
        
    # Save conserved quantities
    fname='%s/Mass.txt' % (dirOutputs)
    with open(fname, 'ab') as opat:
        np.savetxt(opat, [[step*dt, mass]])
        
    fname='%s/Momentum.txt' % (dirOutputs)
    with open(fname, 'ab') as opat:
        np.savetxt(opat, [[step*dt, px]])
        
    fname='%s/Energy.txt' % (dirOutputs)
    with open(fname, 'ab') as opat:
        np.savetxt(opat, [[step*dt, lin_energy+nl_energy, lin_energy, nl_energy]])


# Save field psi
if np.mod(step, psiSave)==0:
    fname='%s/psi%05d.npy' % (dirFields, step/psiSave)
    with open(fname, 'ab') as opab:
        np.save(opab, psi)
    print(fname)
        
    filename='%s/density%05d.png' % (dirPlots, step/psiSave)
    fig=plt.figure()
    plt.plot(x, np.square(np.abs(psi)))
    plt.xlabel('nx')
    plt.ylabel('|psi|^2')
    plt.ylim([0, 1.2])
    plt.savefig(filename)
    plt.close('all')
    
    filename='%s/phase%05d.png' % (dirPlots, step/psiSave)
    fig=plt.figure()
    plt.plot(x, np.angle(psi))
    plt.xlabel('nx')
    plt.ylabel('arg(psi)')
    plt.savefig(filename)
    plt.close('all')


# NR loop
print('NR loop begins')
for step in range(initialStep+1, finalStep+1):
    print('step=', step)

    # Compute right hand side for bicgstab
    b=F(psi)
    n=np.linalg.norm(b)
    print('norm=', n)
    if(n<=tolerance):
        break


    # Run bicgstab method
    #print(LinearOperator((2*nx, 2*nx), matvec=A, dtype=complex))
    deltaPsiPsiStar, info=bicgstab(LinearOperator((2*nx, 2*nx), matvec=A, dtype=complex), b)
    print(info)
    print(np.linalg.norm(deltaPsiPsiStar))
    #print(deltaPsiPsiStar)
    #print(deltaPsiPsiStar.shape, deltaPsiPsiStar.dtype)
 
    # Update initial guess
    psi=psi+deltaPsiPsiStar[0:nx]
    
    
    # Save conserved quantities
    if np.mod(step, conSave)==0:
        
        # Mass
        mass=np.sum(np.abs(psi)**2)*dx
        
        # Nonlinear energy
        nl_energy=-(alpha/2.)*np.sum(np.abs(psi)**4)*dx
        
        # Transform forward into Fourier space (in-place transform)
        psi=np.fft.fft(psi)

        # Linear energy
        lin_energy=beta*np.sum(kSquare*np.abs(psi)**2)*dx
    
        # Linear momentum components
        px=beta*np.sum(kx*np.abs(psi)**2)*dx
    
        # Transform backward into physical space (in-place transform)
        psi=np.fft.ifft(psi)
        
        # Save conserved quantities
        fname='%s/Mass.txt' % (dirOutputs)
        with open(fname, 'ab') as opat:
            np.savetxt(opat, [[step*dt, mass]])
        
        fname='%s/Momentum.txt' % (dirOutputs)
        with open(fname, 'ab') as opat:
            np.savetxt(opat, [[step*dt, px]])
        
        fname='%s/Energy.txt' % (dirOutputs)
        with open(fname, 'ab') as opat:
            np.savetxt(opat, [[step*dt, lin_energy+nl_energy, lin_energy, nl_energy]])


    # Save field psi
    if np.mod(step, psiSave)==0:
        fname='%s/psi%05d.npy' % (dirFields, step/psiSave)
        with open(fname, 'ab') as opab:
            np.save(opab, psi)
        print(fname)

        filename='%s/density%05d.png' % (dirPlots, step/psiSave)
        fig=plt.figure()
        plt.plot(x, np.square(np.abs(psi)))
        plt.xlabel('nx')
        plt.ylabel('|psi|^2')
        plt.ylim([0, 1.2])
        plt.savefig(filename)
        plt.close('all')
        
        filename='%s/phase%05d.png' % (dirPlots, step/psiSave)
        fig=plt.figure()
        plt.plot(x, np.angle(psi))
        plt.xlabel('nx')
        plt.ylabel('arg(psi)')
        plt.savefig(filename)
        plt.close('all')


# End of integration loop
print('NR loop ends')








