## 1D NLS Python

Python code to integrate (evolve in time) the one-dimensional nonlinear Schroedinger (NLS) equation.
Periodic boundary conditions are assumed and the integration is performed using a time-splitting where the spatial derivatives are computed using spectral methods.

Specifically, the equation solved reads:

[//]: <> (i\frac{\partial\psi}{\partial t} + \beta\frac{\partial^2\psi}{\partial x^2} + \alpha|\psi|^2\psi = 0 \, .)
![NLS equation](https://latex.codecogs.com/png.download?%5Clarge%20i%5Cfrac%7B%5Cpartial%5Cpsi%7D%7B%5Cpartial%20t%7D%20+%20%5Cbeta%5Cfrac%7B%5Cpartial%5E2%5Cpsi%7D%7B%5Cpartial%20x%5E2%7D%20+%20%5Calpha%7C%5Cpsi%7C%5E2%5Cpsi%20%3D%200%20%5C%2C%20.)

The code outputs the wave field psi at different time steps, and the conserved quantities of the system (mass, energy, and linear momentum).
They read, respectively:

[//]: <> (M = \int |\psi|^2 dx \, ,)
![mass equation](https://latex.codecogs.com/png.download?%5Clarge%20M%20%3D%20%5Cint%20%7C%5Cpsi%7C%5E2%20dx%20%5C%2C%20%2C)

[//]: <> (E = E_{lin} + E_{nl} = \beta \int \left|\frac{\partial \psi}{\partial x}\right|^2 dx - \frac{\alpha}{2} \int |\psi|^4 dx \, ,)
![energy equation](https://latex.codecogs.com/png.download?%5Clarge%20E%20%3D%20E_%7Blin%7D%20+%20E_%7Bnl%7D%20%3D%20%5Cbeta%20%5Cint%20%5Cleft%7C%5Cfrac%7B%5Cpartial%20%5Cpsi%7D%7B%5Cpartial%20x%7D%5Cright%7C%5E2%20dx%20-%20%5Cfrac%7B%5Calpha%7D%7B2%7D%20%5Cint%20%7C%5Cpsi%7C%5E4%20dx%20%5C%2C%20%2C)

[//]: <> (P = -i\beta \int \left(\psi^\ast\frac{\partial \psi}{\partial x} -\psi\frac{\partial \psi^\ast}{\partial x} \right) dx \, .)
![linear momentum equation](https://latex.codecogs.com/png.download?%5Clarge%20P%20%3D%20-i%5Cbeta%20%5Cint%20%5Cleft%28%5Cpsi%5E%5Cast%5Cfrac%7B%5Cpartial%20%5Cpsi%7D%7B%5Cpartial%20x%7D%20-%5Cpsi%5Cfrac%7B%5Cpartial%20%5Cpsi%5E%5Cast%7D%7B%5Cpartial%20x%7D%20%5Cright%29%20dx%20%5C%2C%20.)

A handwritten note with the details of the time-splitting and spectral methods used is also attached [here](./notes.pdf).
